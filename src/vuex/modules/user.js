import axios from '../../axios/http'
import router from '../../router/index'
const user = {
  state: {
    name: '',
    adminId: 0,
    companyId: 0,
    menus: [],
    status: 'login',
    session: null,
    phone: ''
  },
  mutations: {
    userInfo(state, payload) {
      let data = payload.data;
      state.adminId = data.id === undefined ? 0: data.id;
      state.companyId = data.companyId === undefined ? 0 : data.companyId;
      state.name = data.name === undefined ? '' : data.name;
      state.phone = data.phone === undefined ? '' : data.phone;
    },
    menuList(state, payload) {
      state.menus = payload.menus
    },
    checkSession(state, payload) {
      axios.post('/admin/GetMySession.do', {})
        .then((response) => {
          let result_data = response.data;
          state.session = result_data.status;
        })
        .catch((error) => {
          console.log(error.message)
        })
    }
  },
  actions: {

  }
}

export default user