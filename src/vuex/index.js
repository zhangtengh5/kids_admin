import Vue from 'vue'
import Vuex from 'vuex'
import user from './modules/user'
import path from './modules/path'
Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    user,
    path
  },
  getters: {
    session(state, getters) {
      return state.user.session
    },
    phone(state, getters) {
      return state.user.phone
    },
    adminId(state, getters) {
      return state.user.adminId
    },
    companyId(state, getters) {
        return state.user.companyId
    },
    menus(state, getters) {
      return state.user.menus
    },
    name(state, getters) {
      return state.user.name
    },
    status(state, getters) {
      return state.user.status
    },
    topath(state, getters) {
      return state.path.topath
    },
    frompath(state, getters) {
      return state.path.frompath
    },
    collapse (state, getters) {
      return state.path.collapse
    }
  },
  mutations: {
    
  },
  actions: {

  }
})

export default store
